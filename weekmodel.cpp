#include <QDate>
#include "global.h"
#include "weekmodel.h"

WeekModel::WeekModel(QObject *parent)
    : QAbstractItemModel(parent)
{

}

void WeekModel::setWeekData(const QVector<QPair<QDateTime, QDateTime>> & a_data)
{
    // a_data might not have 5 entries
    m_data.fill(QPair<QDateTime, QDateTime>{}, DAYS_PER_WEEK);

    for (const auto & pair : a_data) {
        if (false == pair.first.isNull()) {
            m_data[pair.first.date().dayOfWeek() - 1] = pair;
        } else {
        }
    }

    // Fill the rest of the model with pairs of dates, so that whole week
    // is covered with date information.
    QDate lastKnownDate;
    for (int i = 0; i < m_data.size(); ++i) {
        if (m_data[i].first.isNull()) {
            lastKnownDate = lastKnownDate.addDays(1);
            QPair<QDateTime, QDateTime> nullPair = { QDateTime{lastKnownDate}, QDateTime{lastKnownDate} };
            m_data[i] = nullPair;
        } else {
            lastKnownDate = m_data[i].first.date();
        }
    }

    invalidateCache();
    dataChanged(createIndex(0, 0), createIndex(DAYS_PER_WEEK, 0));
}

// returns number of days for which we don't have valid values this week
// which are before the argument (excluding before date)
int WeekModel::numbersOfDaysOff(const QDate & before) const
{
    int offDays = 0;
    for (const auto & pair : m_data) {
        if (pair.first.date() == before) {
            break;
        }
        if (pair.first.isNull()) {
            offDays++;
        }
    }
    return offDays;
}

// Updates end time for given day with a new time value
void WeekModel::updateEndOfDay(const QDateTime &a_endOfTheDay)
{
    const int dayIndex = a_endOfTheDay.date().dayOfWeek() - 1;
    m_data[dayIndex].second = a_endOfTheDay;
    m_cachedHours[dayIndex] = 0;

    auto modelIndex = createIndex(dayIndex, 0);
    auto totalModelIndex = createIndex(m_cachedHours.size(), 0);
    dataChanged(modelIndex, modelIndex);
    dataChanged(totalModelIndex, totalModelIndex);
}

double WeekModel::calculateTotalTime() const
{
    return std::accumulate(m_cachedHours.begin(), m_cachedHours.end(), 0.0);
}

QVariant WeekModel::data(const QModelIndex &index, int role) const
{
    Q_UNUSED(role)
    QVariant retValue{};

    if ((index.column() == 0) && (false == m_data.empty())) {
        switch (role) {
            case Qt::DisplayRole:
                if ((index.row() < m_cachedHours.size() && (index.row() < m_data.size()))) {
                    if (qFuzzyIsNull(m_cachedHours[index.row()]) == false) {
                        retValue = m_cachedHours[index.row()];
                    } else {
                        const auto & pair = m_data[index.row()];
                        double hoursWorked = 0;
                        if (pair.first.isValid() && pair.second.isValid()) {
                            const auto elapsedTimeInSeconds =
                                    pair.first.time().secsTo(
                                        pair.second.time()
                                    );
                            const QTime t = QTime(0,0).addSecs(elapsedTimeInSeconds);
                            hoursWorked = double(t.hour()) + t.minute() / 60.0 + t.second() / 3600.0;
                            m_cachedHours[pair.first.date().dayOfWeek() - 1] = hoursWorked;
                        }
                        retValue = hoursWorked;
                    }
                } else {
                    // TODO: cache this too?
                    retValue = calculateTotalTime();
                }
                break;
            case Qt::UserRole:  // Date or week number
                if ((index.row() < m_data.size())) {
                    retValue = m_data[index.row()].first.date();
                } else {
                    for (int i = 0; i < m_data.size(); i++) {
                        if (m_data[i].first.isValid()) {
                            retValue = m_data[i].first.date().weekNumber();
                            break;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    return retValue;
}

QVariant WeekModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(role)
    QVariant retValue{};
    if (orientation == Qt::Vertical)
        if (section  < m_weekdayNames.size())
            retValue = m_weekdayNames[section];
        else
            retValue = "TOTAL";
    else
        retValue = section;

    return retValue;
}

int WeekModel::columnCount(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return 1;
}

int WeekModel::rowCount(const QModelIndex &index) const
{
    Q_UNUSED(index)
    return m_cachedHours.size() + 1;
}


QModelIndex WeekModel::index(int row, int column,
                             const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return createIndex(row, column);
}

QModelIndex WeekModel::parent(const QModelIndex &child) const
{
    Q_UNUSED(child)
    return QModelIndex();
}
