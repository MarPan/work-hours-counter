#include <QDebug>
#include <QDate>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QHeaderView>
#include <QScrollBar>
#include <QLabel>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QTableView>
#include <QCheckBox>
#include <QEvent>
#include <QMenu>
#include <QPainter>
#include <QPushButton>
#include <QCloseEvent>
#include <QApplication>
#include "global.h"
#include "weekview.h"
#include "weekmodel.h"
#include "mainwindow.h"
#include "Windows.h"
#include "windef.h"
#include "WinUser.h"
#include "WtsApi32.h"

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_dayStart(QDateTime::currentDateTime())
{
    bool registered = WTSRegisterSessionNotification(HWND(winId()), NOTIFY_FOR_ALL_SESSIONS);
    if (registered == false) {
        QApplication::instance()->exit(-1);
    }

    qInstallMessageHandler(myMessageOutput);

    // ------------------ WIDGETS --------------------
    m_textEdit = new QTextEdit{this};
    m_tray = new QSystemTrayIcon{this};
    m_timeWorkedToday = new QLabel{};
    m_timeLeftInWeek = new QLabel{};
    m_prognosis = new QLabel{};
    m_timeOfEnd = new QLabel{};
    m_labelWidget = new QWidget{};
    m_labelLayout = new QVBoxLayout{};
    m_tableView = new QTableView{};
    m_tabWidget = new QTabWidget{this};
    m_continousUpdate = new QCheckBox{"Continuous updates"};

    // "Time worked today" tab
    m_timeWorkedToday->setFont(QFont("", 24, 10));
    m_labelLayout->addStretch();
    m_labelLayout->addWidget(new QLabel("You are working for"), 0, Qt::AlignCenter);
    m_labelLayout->addWidget(m_timeWorkedToday, 0, Qt::AlignCenter);
    m_labelLayout->addWidget(m_timeLeftInWeek, 0, Qt::AlignCenter);
    m_labelLayout->addWidget(m_prognosis, 0, Qt::AlignCenter);
    m_labelLayout->addWidget(m_timeOfEnd, 0, Qt::AlignCenter);
    m_labelLayout->addStretch();
    m_labelWidget->setLayout(m_labelLayout);

    // Databse tab
    QWidget *dbCentralWidget = new QWidget{};
    auto *refreshButton = new QPushButton{"Refresh"};
    connect(refreshButton, &QPushButton::clicked, [=]() {  });
    auto layout = new QVBoxLayout{};
    dbCentralWidget->setLayout(layout);
    m_continousUpdate->setCheckState(Qt::CheckState::Checked);
    layout->addWidget(m_continousUpdate);
    layout->addWidget(m_tableView);


    //          Week overview tab
    //
    //          <-    Current   ->
    //
    //   ****  ****  ****  ****  ****  ****
    //   *  *  *  *  *  *  *  *  *  *  *  *
    //   ****  ****  ****  ****  ****  ****
    //
    auto weekViewWidget = new QWidget{};
    auto weekTableView = new QTableView{};
    auto wvlayout = new QVBoxLayout{};
    auto pushButtonsLayout = new QHBoxLayout{};
    auto previousWeekPb = new QPushButton{"<-"};
    auto currentWeekPb = new QPushButton{"Current"};
    auto nextWeekPb = new QPushButton{"->"};
    m_weekView = new WeekView{};
    m_currentWeekModel = new WeekModel{};
    pushButtonsLayout->addWidget(previousWeekPb);
    pushButtonsLayout->addWidget(currentWeekPb);
    pushButtonsLayout->addWidget(nextWeekPb);
    weekTableView->setModel(m_currentWeekModel);
    m_weekView->setModel(m_currentWeekModel);
    wvlayout->addLayout(pushButtonsLayout);
    wvlayout->addWidget(m_weekView, 0, Qt::AlignCenter);
    weekViewWidget->setLayout(wvlayout);
    connect(previousWeekPb, &QPushButton::clicked, this, &MainWindow::showPreviousWeek);
    connect(currentWeekPb, &QPushButton::clicked, this, &MainWindow::showCurrentWeek);
    connect(nextWeekPb, &QPushButton::clicked, this, &MainWindow::showNextWeek);
    connect(m_weekView, &WeekView::vacationStatusChanged, this, &MainWindow::onVacationStatusChanged);

    // Add the tabs to the tab widget
    m_tabWidget->addTab(m_labelWidget, "Time");
    m_tabWidget->addTab(dbCentralWidget, "Database");
    m_tabWidget->addTab(m_textEdit, "Log");
    m_tabWidget->addTab(weekViewWidget, "Week");
    setCentralWidget(m_tabWidget);

    // ------------------ DATABASE --------------------
    const QString DRIVER("QSQLITE");
    if (QSqlDatabase::isDriverAvailable(DRIVER)) {
        m_db = QSqlDatabase::addDatabase(DRIVER);
        m_db.setDatabaseName(DB_FILENAME);
        if (!m_db.open()) {
            qWarning() << "ERROR: " << m_db.lastError();
        } else {
            QSqlQuery query(QString("CREATE TABLE IF NOT EXISTS %1 (id INTEGER NOT NULL PRIMARY KEY, %2 TEXT, %3 TEXT, %4 INTEGER)")
                            .arg(TABLE_NAME).arg(DAY_START_COLUMN).arg(DAY_END_COLUMN).arg(IS_VACATION_COLUMN));
        }
    } else {
        qWarning() << "No " << DRIVER << " driver found";
    }

    // -------- INITIAL STATE OF MODELS & VIEWS -----------
    weekTableView->verticalHeader()->hide();

    m_model = new WorkHoursTableModel(this, m_db);
    m_model->setEditStrategy(QSqlTableModel::EditStrategy::OnManualSubmit);
    m_model->setTable(TABLE_NAME);
    m_model->setSort(0, Qt::DescendingOrder);
    m_model->select();
    onDatabaseUpdated();

    auto submitAction = new QAction{"Commit", m_tableView};
    connect(submitAction, &QAction::triggered, m_model, &QSqlTableModel::submitAll);
    connect(submitAction, &QAction::triggered, this, &MainWindow::readDayStartTime);
    auto revertAction = new QAction{"Revert", m_tableView};
    connect(revertAction, &QAction::triggered, m_model, &QSqlTableModel::revertAll);
    auto refreshAction = new QAction{"Refresh", m_tableView};
    connect(refreshAction, &QAction::triggered, this, &MainWindow::onDatabaseUpdated);
    m_tableView->insertActions(nullptr, {submitAction, revertAction, refreshAction});
    m_tableView->setContextMenuPolicy(Qt::ActionsContextMenu);

    int columnWidth = 115;
    m_tableView->setModel(m_model);
    m_tableView->setColumnWidth(1, columnWidth);
    m_tableView->setColumnWidth(2, columnWidth);
    m_tableView->setColumnWidth(3, int(columnWidth / 1.8));
    m_tableView->hideColumn(0); // don't show the ID

    int columnCount = m_tableView->horizontalHeader()->count();
    int scrollBarWidth = m_tableView->verticalScrollBar()->height();
    int columnTotalWidth = 0;

    for (int i = 0; i < columnCount; ++i) {
        if (false == m_tableView->horizontalHeader()->isSectionHidden(i)) {
            columnTotalWidth += m_tableView->horizontalHeader()->sectionSize(i);
        }
    }
    m_tableView->setMinimumWidth(columnTotalWidth + scrollBarWidth);
    adjustSize();

    readDayStartTime();

    qDebug() << "Current time according to SQLite:";
    QSqlQuery time("SELECT DATETIME('now', 'localtime')");
    while (time.next()) {
        qDebug() << time.value(0).toString();
    }
    updateClock();

    //  -------------------- ICON -------------------
    // Icon is drawn on a transparent background twice.
    // First time with a black big pen, and than with a white smaller one
    // This gives an effect of a white shape with black outline.
    QPixmap pixmap(32, 32);
    pixmap.fill(Qt::transparent);
    QPainter painter{&pixmap};
    QList<QPen> pens = { QPen{QBrush{Qt::black}, 6},
                         QPen{QBrush{Qt::white}, 4} };
    /*
     * P1            P5 |  upperY
     *   \    P3    /   |  middle
     *    \  /  \  /    |
     *     P2    P4     |  lowerY
     */
    const int size = 32, margin = 8, spread = 6;
    const int upperY = margin, middle = size / 2, lowerY = size - margin;
    const QPoint P1{0 + margin, upperY};
    const QPoint P2{middle - spread, lowerY};
    const QPoint P3{middle, middle - 6};
    const QPoint P4{middle + spread, lowerY};
    const QPoint P5{size - margin, upperY};
    for (const auto & pen : pens) {
        painter.setPen(pen);
        painter.drawLine(P1, P2);
        painter.drawLine(P2, P3);
        painter.drawLine(P3, P4);
        painter.drawLine(P4, P5);
    }

    // ---------------------------------------------------

    m_tray->setIcon(pixmap);
    auto menu = new QMenu{};
    menu->addAction("Terminate", QApplication::instance(), &QApplication::quit);
    m_tray->setContextMenu(menu);
    m_tray->show();

    setWindowTitle("Work Hours Counter");
    setWindowIcon(QIcon{pixmap});

    connect(m_tray, &QSystemTrayIcon::activated, this, &MainWindow::onSystemTrayClicked);
    connect(this, &MainWindow::databaseUpdated, this, &MainWindow::onDatabaseUpdated);
    connect(&m_timer, &QTimer::timeout, this, &MainWindow::updateClock);

    m_timer.start(1000);
}

void MainWindow::onDatabaseUpdated()
{
    if (m_weekToBeDisplayed == 0) {
        fillWeekDataModel(m_weekToBeDisplayed);
    }

    QSqlQuery select(QString("SELECT * FROM %1 WHERE %2 >= DATE('now', 'localtime', 'start of day')")
                     .arg(TABLE_NAME).arg(DAY_START_COLUMN));
    while (select.next()) {
        m_dayStart = select.value(1).toDateTime();
    }
    m_model->select();
}

void MainWindow::fillWeekDataModel(int a_week)
{
    if (m_model->isDirty()) {
        qDebug() << "Commiting changes in the main model";
        m_model->submitAll();
    }

    // fetch all entries starting from Monday this week
    QSqlQuery selectWeek(QString("SELECT * FROM %1 WHERE %2 BETWEEN DATE('now', 'localtime',  'weekday 0', '%3 days') AND \
                                                                    DATE('now', 'localtime',  'weekday 0', '%4 days')")
                        .arg(TABLE_NAME).arg(DAY_START_COLUMN).arg(-6 + 7 * a_week).arg(-1 + 7 * a_week));

    QVector<QPair<QDateTime, QDateTime>> data;
    data.reserve(DAYS_PER_WEEK);
    while (selectWeek.next()) {
        data.push_back(qMakePair(selectWeek.value(1).toDateTime(), selectWeek.value(2).toDateTime()));
    }

    if (a_week == 0) {
        m_currentWeekModel->setWeekData(data);
        m_weekView->setModel(m_currentWeekModel);
    } else {
        auto weekModel = new WeekModel{};
        weekModel->setWeekData(data);
        m_weekView->setModel(weekModel);
    }
}

void MainWindow::showPreviousWeek()
{
    m_weekToBeDisplayed--;
    fillWeekDataModel(m_weekToBeDisplayed);
}
void MainWindow::showCurrentWeek()
{
    m_weekToBeDisplayed = 0;
    fillWeekDataModel(m_weekToBeDisplayed);
}
void MainWindow::showNextWeek()
{
    m_weekToBeDisplayed++;
    fillWeekDataModel(m_weekToBeDisplayed);
}

void MainWindow::onVacationStatusChanged(QDate a_affectedDay, bool a_status)
{
    QSqlQuery query(QString("UPDATE %1 SET %2 = %3 WHERE DATE(%4) = '%5'")
                            .arg(TABLE_NAME).arg(IS_VACATION_COLUMN)
                            .arg(int(a_status)).arg(DAY_START_COLUMN)
                            .arg(a_affectedDay.toString(SQLITE_DATE_FORMAT)));
    emit databaseUpdated();

    // Well, shit. I want to mark future days about being vacation days or not.
    // But there are no entries for that day yet!

    // Can I add future days, with empty dayStart and dayEnds?
    // To do that, I have to get rid of the code, that references the very top record
    //   in the model as the current day first.
}

void MainWindow::readDayStartTime()
{
    QSqlQuery select(QString("SELECT * FROM %1 WHERE %2 >= DATE('now', 'localtime', 'start of day')")
                     .arg(TABLE_NAME).arg(DAY_START_COLUMN));
    while (select.next()) {
        m_dayStart = select.value(1).toDateTime();
        qDebug() << "Fetched start of the day from the database " << m_dayStart.toString("hh:mm:ss");
    }
}


void MainWindow::updateTodaysEndime(QDateTime a_datetime)
{
    QSqlQuery query(QString("UPDATE %1 SET %2 = '%3' WHERE DATE(%4) = '%5'")
                            .arg(TABLE_NAME).arg(DAY_END_COLUMN)
                            .arg(a_datetime.toString(SQLITE_DATE_TIME_FORMAT)).arg(DAY_START_COLUMN)
                            .arg(QDate::currentDate().toString(SQLITE_DATE_FORMAT)));
    if (query.lastError().isValid() == true) {
        qDebug() << Q_FUNC_INFO << query.lastError() << query.lastQuery();
    }

    QString dateStr = a_datetime.date().toString(SQLITE_DATE_FORMAT);
    QModelIndexList matchingIndexes = m_model->match(m_model->index(0, 1), Qt::DisplayRole, dateStr, 1);
    if (matchingIndexes.size() > 0) {
        m_model->selectRow(matchingIndexes[0].row());
    }
}

void MainWindow::onSystemTrayClicked(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::ActivationReason::MiddleClick:
            m_tray->showMessage("Hang in there!", "You are working for " + m_timeWorkedToday->text());
            break;
        case QSystemTrayIcon::ActivationReason::Context:
            // ignore right clicks
            break;
        default:
           if (true == isVisible()) {
                hide();
            } else {
                showNormal();
                setWindowState((windowState() & ~Qt::WindowMinimized) | Qt::WindowActive);
            }
            break;
    }
}

void MainWindow::updateClock()
{
    auto const currentTime = QDateTime::currentDateTime();

    // if we haven't even started working yet, do nothing
    if (m_dayStart.isNull() == true) {
        return;
    }

    if (m_sessionActive) {
        // Update the models
        if (m_continousUpdate->isChecked()) {
            updateTodaysEndime(currentTime);
        }
        m_currentWeekModel->updateEndOfDay(currentTime);
    }

    // calculate lots of stuff used later on. Most of this is here just for readability sake.
    const int elapsedTimeInSeconds = m_dayStart.time().secsTo(currentTime.time());
    const int numberOfDaysOff = m_currentWeekModel->numbersOfDaysOff(currentTime.date());
    // --- these are the same as long as date does not change
        const int daysToWorkThisWeek = DAYS_PER_WEEK - numberOfDaysOff;
        const int requiredTimePerWeekInSeconds = daysToWorkThisWeek * SECONDS_PER_DAY;
    // ---
    const int secondsLeft = requiredTimePerWeekInSeconds - (int(m_currentWeekModel->calculateTotalTime() * 60 * 60));
    const double hoursLeft = secondsLeft / 3600.0;
    const int thisWeekTimeInSeconds = int(m_currentWeekModel->calculateTotalTime() * 60 * 60);
    const int secondsToWorkToday = int(daysToWorkThisWeek * SECONDS_PER_DAY  - (thisWeekTimeInSeconds - elapsedTimeInSeconds));

    // Detect date change - this should happen when screen is locked
    if ((m_sessionActive == false) && (currentTime.date() != m_dayStart.date())) {
        // TODO: come back to this when the unit tests are ready
//        // if today was the last day of week and last dayStart entry was today, update the dayEnd date so that we have exactly 40 hours
//        if ((currentTime.date().dayOfWeek() == DAYS_PER_WEEK) && (m_model->index(0,0).data().toDateTime().date() == m_dayStart.date())) {
//            // TODO: change this to query, which updates current date
//            updateTodaysEndime(m_dayStart.addSecs(secondsToWorkToday));
//            emit databaseUpdated();
//        }

        m_timeWorkedToday->clear();
        m_dayStart = QDateTime{};
        return;
    }

    // Calculate how much time was worked today and display it in a nice format
    const QTime elapsedTimeToday = QTime(0,0).addSecs(elapsedTimeInSeconds);
    m_timeWorkedToday->setText(elapsedTimeToday.toString());

    // ----------------- PROGNOSIS --------------------------------
    if (currentTime.date().dayOfWeek() != DAYS_PER_WEEK) {
        QString prognosis = tr(" If you leave now, you will have to work %2 hours per day for the rest of the week.")
            .arg(hoursLeft / (DAYS_PER_WEEK - currentTime.date().dayOfWeek()));
        m_prognosis->setText(prognosis);
        m_prognosis->setVisible(true);
    } else {
        m_prognosis->setVisible(false);
    }
    QString hoursLeftInTheWeek = QString{"%1 hours left in the week."}.arg(hoursLeft);
    m_timeLeftInWeek->setText(hoursLeftInTheWeek);

    /* ----------------- PROJECTED TIME OF LEAVING ---------------------
     * On Mon-Thurs just write when mean hours will be reached
     * On Friday when 40 hours are reached */
    QString endTimeStr;
    QDateTime endTime;
    if (currentTime.date().dayOfWeek() == DAYS_PER_WEEK) {
        endTime = currentTime.addSecs(secondsLeft);
        endTimeStr = tr("You can leave at %1. You will work %2 hours today.").arg(endTime.time().toString()).arg(secondsToWorkToday / 60.0 / 60.0);
    } else {
        /* instead of calculating when I'll reach 8 hours, I should
         * subtract already worked hours from previous day, calculate how much hours per day I should work
         * for the remaining days and use that. */
        int secondsToWorkLeftInTheWeek = daysToWorkThisWeek * SECONDS_PER_DAY - (thisWeekTimeInSeconds - elapsedTimeInSeconds);
        // plus one, because we want to include current day
        int remainingDaysToWork = DAYS_PER_WEEK - currentTime.date().dayOfWeek() + 1;
        int secondsPerRemainingDay = secondsToWorkLeftInTheWeek / remainingDaysToWork;
        double hoursPerRemainingDay =  secondsPerRemainingDay / 60.0 / 60;

        endTime = m_dayStart.addSecs(secondsPerRemainingDay);
        endTimeStr = tr("You will reach %1 hours at %2").arg(hoursPerRemainingDay).arg(endTime.time().toString());
    }
    m_timeOfEnd->setText(endTimeStr);

    if (currentTime == endTime) {
        m_tray->showMessage("Is it time to stop?", "You reached daily goal");
    }
}

MainWindow::~MainWindow()
{
    WTSUnRegisterSessionNotification(HWND(winId()));
}

// React on Windows events regarding the session state: locking screen, logging in/out and so on.
bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType)
    Q_UNUSED(result)

    MSG* msg = reinterpret_cast<MSG*>(message);
    switch(msg->message)
    {
        case WM_WTSSESSION_CHANGE:
            // whatever event happened, it means that I am active.
            auto wmSessionStat = LOWORD(msg->wParam);

            switch (wmSessionStat)
            {
            case WTS_SESSION_UNLOCK:
            case WTS_SESSION_LOGON: {
                qDebug() << "A User logged in or a session is unlocked.";
                m_sessionActive = true;

                // insert new entry only if there are no entries from today
                QSqlQuery select(QString("SELECT DATETIME(dayStart, 'localtime'),DATETIME(dayEnd, 'localtime') FROM %1 WHERE %2 >= DATE('now', 'localtime', 'start of day')")
                                 .arg(TABLE_NAME).arg(DAY_START_COLUMN));
                int numberOfHits = 0;
                while (select.next()) {
                    numberOfHits++;
                }
                if (numberOfHits == 0) {
                    QSqlQuery insert(QString("INSERT INTO %1(%2) VALUES(DATETIME('now', 'localtime'))").arg(TABLE_NAME).arg(DAY_START_COLUMN));
                    m_dayStart = QDateTime::currentDateTime();
                    emit databaseUpdated();
                } else if (numberOfHits > 1) {
                    qWarning() << "Corrupted database. Multiple entries found for today's date.";
                }
                break;
            }
            case WTS_SESSION_LOCK:
            case WTS_SESSION_LOGOFF: {
                qDebug() << "A user is logging out or the session is getting locked.";
                m_sessionActive = false;

                // update dayEnd entry for today's date
                // TODO: alternatively we could just commit the current value in data(0,2), since it
                //       is continuusly updated, but not commited.
                QSqlQuery query(QString("UPDATE %1 SET %2 = DATETIME('now', 'localtime') WHERE %3 >= DATE('now', 'localtime', 'start of day')")
                                        .arg(TABLE_NAME).arg(DAY_END_COLUMN).arg(DAY_START_COLUMN));
                emit databaseUpdated();
                break;
            }
            case WTS_REMOTE_CONNECT:
            case WTS_REMOTE_DISCONNECT:
            default:
                break;
            }
            break;
    }

    return 0;
}

void MainWindow::exportMonthHours(int month)
{
    // lets say today is 11, and I want data for 10.
    // this fails masterfully if the year changes /facepalm
    // Maybe lets just fuck that and hardcode it to get last month
    int monthDiff = month - QDate::currentDate().month();


    // Retrieves entries from the start of the previous month till the end of the previous month
    QSqlQuery selectMonth(QString("SELECT * FROM %1 WHERE %2 BETWEEN DATE('now', 'localtime',  'start of month', '-1 month') AND \
                                                                     DATE('now', 'localtime',  'start of month', '0 month', '-1 day')"));
}

// React on minimization attempt and hide the windw, so only the tray icon is displayed.
void MainWindow::changeEvent(QEvent* e)
{
    switch (e->type()) {
        case QEvent::WindowStateChange:
            if (this->windowState() & Qt::WindowMinimized) {
                hide();
            }
            break;
        default:
            break;
    }

    QMainWindow::changeEvent(e);
}

// React on close window attempt and hide the window.
void MainWindow::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
}

void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    auto now = QTime::currentTime();
    QString printable = QString("[%1] ").arg(now.toString("hh:mm:ss"));
    printable += msg;

    if (MainWindow::m_textEdit) {
        MainWindow::m_textEdit->append(printable);
    }

    QByteArray localMsg = msg.toLocal8Bit();
    const char *file = context.file ? context.file : "";
    const char *function = context.function ? context.function : "";

    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtInfoMsg:
        fprintf(stderr, "Info: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);

        break;
    case QtWarningMsg:
        fprintf(stderr, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), file, context.line, function);
        break;
    }
}
