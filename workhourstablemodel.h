#ifndef WORKHOURSTABLEMODEL_H
#define WORKHOURSTABLEMODEL_H

#include <QSqlTableModel>

class WorkHoursTableModel : public QSqlTableModel
{
    Q_OBJECT
public:
    explicit WorkHoursTableModel(QObject *parent = nullptr,
                                 QSqlDatabase a_db = QSqlDatabase{});

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int columnCount(const QModelIndex &index = QModelIndex()) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
};

#endif // WORKHOURSTABLEMODEL_H
