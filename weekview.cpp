#include <QGridLayout>
#include <QLabel>
#include <QStandardItemModel>
#include <QToolButton>
#include <QSizePolicy>
#include <QDebug>
#include "weekview.h"

WeekView::WeekView(QWidget *parent) : QWidget(parent)
{
    m_gridLayout= new QGridLayout{};
    m_gridLayout->setVerticalSpacing(0);

    // 5 entries for each weekday, and one for TOTAL
    const int rowNumber = 3;
    m_labels = {rowNumber, QVector<QToolButton*>{}};

    // row 0 contains weekDay names (fetched from model headerData)
    // row 1 contains hour values for each day (fetched from model data, displayrole)
    // row 2 contains calendar dates for each day (fetched from model data, userrole)
    for (int row = 0 ; row < rowNumber; ++row)
    {
        for (int column = 0 ; column < 6; ++column)
        {
            auto toolButton  = new QToolButton{};
            // apply styling
            QString styleString;
            if (row == 0) {
                styleString = "QToolButton { background-color: tomato; border: 1px solid black; border-top-right-radius: 4px; border-top-left-radius: 4px; padding: 2px; font-size: 14px}";
            } else {
                styleString = "QToolButton { background-color: white; border: 1px solid black; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; padding: 10px; font-size: 18px}";
                if (row == 1) {
                    toolButton->setCheckable(true);
                    connect(toolButton, &QToolButton::toggled, this, &WeekView::handleMouseClick);
                    styleString = "QToolButton:checked { background-color: red } QToolButton { background-color: white; border: 1px solid black; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; padding: 10px; font-size: 18px}";
                }

            }
            toolButton->setStyleSheet(styleString);

            toolButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
            m_gridLayout->addWidget(toolButton, row, column);
            m_labels[row].push_back(toolButton);
        }
    }
    this->setLayout(m_gridLayout);
}

void WeekView::handleMouseClick(bool a_toogled)
{
    auto senderObject = sender();
    for (int i = 0; i < m_labels[1].size(); ++i) {
        if (senderObject == m_labels[1][i]) {
            qDebug() << m_model->headerData(i, Qt::Vertical).toString() << ": " << m_labels[1][i]->isChecked();
            qDebug() << m_model->index(i,0).data(Qt::UserRole);
            qDebug() << m_model->index(i,0).data(Qt::UserRole).toDate();
            qDebug() << m_model->index(i,0).data(Qt::UserRole).toDate().toString();
            vacationStatusChanged(m_model->index(i,0).data(Qt::UserRole).toDate(), a_toogled);
        }
    }
}

void WeekView::setModel(QAbstractItemModel *a_model)
{
    m_model = a_model;
    connect(a_model, &QAbstractItemModel::dataChanged,
            this, &WeekView::onDataChanged);

    for (int i = 0; i < a_model->rowCount(); i++) {
        m_labels[0][i]->setText(a_model->headerData(i, Qt::Vertical).toString());   // Weekday name
    }
    for (int i = 0; i < a_model->rowCount(); i++) {        
        m_labels[1][i]->setText(QString::number(a_model->index(i,0).data().toDouble(), 'f', 2));  // Time worked
    }
    for (int i = 0; i < a_model->rowCount(); i++) {
        if (i != a_model->rowCount() - 1) {
            m_labels[2][i]->setText(a_model->index(i,0).data(Qt::UserRole).toDate().toString("d MMM"));  // Date
        } else {
            m_labels[2][i]->setText("Week " + a_model->index(i,0).data(Qt::UserRole).toString());  // Week number
        }

    }
}


void WeekView::onDataChanged(const QModelIndex& topLeft,
                             const QModelIndex& bottomRight,
                             const QVector<int>& roles)
{
    Q_UNUSED(roles)
    for (int row = topLeft.row(); row <= bottomRight.row(); row++)
    {
        for (int col = topLeft.column(); col <= bottomRight.column(); col++)
        {
            // the model has one column and 6 rows
            auto variant = m_model->index(row, col).data();
            m_labels[1][row]->setText(QString::number(variant.toDouble(), 'f', 2));
        }
    }
}
