#ifndef GLOABL_H
#define GLOABL_H

#include <QString>

const int DAYS_PER_WEEK = 5;
const int HOURS_PER_DAY = 8;
const int SECONDS_PER_DAY = 8 * 60 * 60;
const QString SQLITE_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
const QString SQLITE_DATE_FORMAT = "yyyy-MM-dd";

#endif // GLOABL_H
