#ifndef WEEKVIEW_H
#define WEEKVIEW_H

#include <QWidget>
#include <QModelIndex>
#include <QVector>
#include <QDate>

class QGridLayout;
class QToolButton;

/*!
 * \brief The WeekView class displays data in a series of labels.
 * Floating point numbers are rounded to two decimal points.
 */
class WeekView : public QWidget
{
    Q_OBJECT
public:
    explicit WeekView(QWidget *parent = nullptr);

    void setModel(QAbstractItemModel* a_model);

signals:
    void vacationStatusChanged(QDate a_affectedDay, bool a_status);

private slots:
    void onDataChanged(const QModelIndex& topLeft,
                       const QModelIndex& bottomRight,
                       const QVector<int>& roles = QVector<int>{});
    void handleMouseClick(bool a_toogled);

private:
    QAbstractItemModel* m_model;
    QGridLayout* m_gridLayout;
    QVector<QVector<QToolButton*>> m_labels;
};

#endif // WEEKVIEW_H
