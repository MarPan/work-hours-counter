#ifndef WEEKMODEL_H
#define WEEKMODEL_H

#include <QAbstractItemModel>
#include <QDateTime>

class WeekModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit WeekModel(QObject *parent = nullptr);

    void setWeekData(const QVector<QPair<QDateTime, QDateTime>> & a_data);

    void updateEndOfDay(const QDateTime & a_endOfTheDay);
    double calculateTotalTime() const;
    int numbersOfDaysOff(const QDate & before) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    int columnCount(const QModelIndex & index = QModelIndex()) const override;
    int rowCount(const QModelIndex & index = QModelIndex()) const override;

    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &child) const override;

private:
    void invalidateCache() { m_cachedHours = { 0, 0, 0, 0, 0 }; }
    mutable QVector<double> m_cachedHours = { 0, 0, 0, 0, 0 };
    QVector<QPair<QDateTime, QDateTime>> m_data;
    static inline const QStringList m_weekdayNames = {"Monday", "Tuseday", "Wednsday", "Thursday", "Friday"};
};

#endif // WEEKMODEL_H
