#include <QDateTime>
#include <QBrush>
#include "global.h"
#include "workhourstablemodel.h"

WorkHoursTableModel::WorkHoursTableModel(QObject *a_parent,
                                         QSqlDatabase a_db)
    : QSqlTableModel(a_parent, a_db)
{

}

QVariant WorkHoursTableModel::data(const QModelIndex &a_index, int a_role/* = Qt::DisplayRole*/) const
{
    if ((a_index.column() == QSqlTableModel::columnCount()) && (a_role == Qt::DisplayRole)) {
        auto elapsedTimeInSeconds = data(index(a_index.row(), 1)).toDateTime()
                .time().secsTo(
                    data(index(a_index.row(), 2)).toDateTime().time());
        QTime t = QTime(0,0).addSecs(elapsedTimeInSeconds);
        return QVariant(t.toString("hh:mm:ss"));
    } else if (a_role == Qt::TextAlignmentRole) {
        return QVariant(Qt::AlignCenter);
    } else if (a_role == Qt::BackgroundRole && isDirty(a_index) && (a_index.column() < QSqlTableModel::columnCount())) {
        return QVariant(QBrush(QColor(Qt::yellow)));
    } else {
        return QSqlTableModel::data(a_index, a_role);
    }
}

QVariant WorkHoursTableModel::headerData(int a_section, Qt::Orientation a_orientation, int a_role /*= Qt::DisplayRole*/) const
{
    if ((a_section == QSqlTableModel::columnCount()) && (a_role == Qt::DisplayRole) && (a_orientation == Qt::Horizontal)) {
        return QVariant("Work time");
    } else {
        return QSqlTableModel::headerData(a_section, a_orientation, a_role);
    }
}
int WorkHoursTableModel::columnCount(const QModelIndex &index/*= QModelIndex()*/) const
{
    int returnValue = 0;
    if (false == index.isValid()) {
        returnValue = QSqlTableModel::columnCount() + 1;
    } else {
        returnValue = QSqlTableModel::columnCount();
    }
    return returnValue;
}

// Any changes to a row means that the magical last column needs to be updated.
bool WorkHoursTableModel::setData(const QModelIndex &index, const QVariant &value, int role/* = Qt::EditRole*/)
{
    auto retValue = QSqlTableModel::setData(index, value, role);
    emit dataChanged(this->index(index.row(), columnCount()), this->index(index.row(), columnCount()), {role});
    return retValue;
}
