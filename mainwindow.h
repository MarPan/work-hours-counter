#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QDateTime>
#include <QSystemTrayIcon>
#include "workhourstablemodel.h"

class WeekModel;
class WeekView;
class QLabel;
class QVBoxLayout;
class QTextEdit;
class QTableView;
class QCheckBox;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;
    static inline QTextEdit * m_textEdit = nullptr;

private: // variables
    QSqlDatabase m_db;

    QLabel * m_timeWorkedToday;
    QLabel * m_timeLeftInWeek;
    QLabel * m_prognosis;
    QLabel * m_timeOfEnd;
    QWidget * m_labelWidget;
    QVBoxLayout * m_labelLayout;
    QTableView * m_tableView;
    QTabWidget * m_tabWidget;
    WorkHoursTableModel * m_model;
    QCheckBox * m_continousUpdate;

    /* */
    WeekModel * m_currentWeekModel ;
    WeekView * m_weekView;
    QSystemTrayIcon * m_tray;

    // The application clock. Triggers main view recalculation every second.
    QTimer m_timer;

    // Stores the time when the work started, for the current day.
    QDateTime m_dayStart;

    /* 0 means current week. For example, -1 is the previous week. */
    int m_weekToBeDisplayed = 0;

    // Denotes if the user is curenlty logged in or not.
    bool m_sessionActive = true;

    static inline const QString DAY_START_COLUMN = "dayStart";
    static inline const QString DAY_END_COLUMN = "dayEnd";    
    static inline const QString IS_VACATION_COLUMN = "isVacation";
    static inline const QString TABLE_NAME = "hours";
    static inline const QString DB_FILENAME = "hours.db";

private:
    void changeDisplayedWeek();
    void fillWeekDataModel(int a_week);
    void exportMonthHours(int month);
    void updateTodaysEndime(QDateTime a_datetime);

private slots:
    void updateClock();
    void onSystemTrayClicked(QSystemTrayIcon::ActivationReason reason);
    void onDatabaseUpdated();
    void readDayStartTime();

    void showPreviousWeek();
    void showCurrentWeek();
    void showNextWeek();

    void onVacationStatusChanged(QDate a_affectedDay, bool a_status);

signals:
    void databaseUpdated();

protected:
    bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;
    void changeEvent(QEvent* e) override;
    void closeEvent(QCloseEvent *event) override;
};
#endif // MAINWINDOW_H
